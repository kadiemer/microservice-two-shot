# Wardrobify

Team:

* Matthew Rauschenberg - Hats Microservice
* Kadie Meroney - Shoes Microservice

## Design

## Shoes microservice

The Shoes RESTful API includes two models, the Shoe model and the BinVO model. The BinVO pulls data from the existing wardrobe API via a polling service, which checks for new bins in the wardrobe api and updates the BinVo model as necessary. Then using React, the frontend was created to allow users to add, view, and delete shoes in the database through HTTP requests.

## Hats microservice

The Hats RESTful API includes two models, the Hat model and the LocationVO model, The Location model allows you to put hats on shelves in sections of the wardrobes across different rooms with different wardrobes. The LocationVO model allows you to create new wardrobes as needed in new rooms. I then displayed the hats using React. In my React front end I am not just able to view the hats but I can also create and delete hats.
