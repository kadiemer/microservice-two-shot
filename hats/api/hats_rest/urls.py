from django.urls import path
from .views import list_hats, details_hats


urlpatterns = [
    path("hats/", list_hats, name="list_hats"),
    path('hats/<int:pk>/', details_hats, name="detail_hats"),
]
