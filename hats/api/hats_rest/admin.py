from django.contrib import admin
from .models import LocationVO, Hat

# Register your models here.


@admin.register(LocationVO)
class LocationVOadmin(admin.ModelAdmin):
    pass


@admin.register(Hat)
class HatsAdmin(admin.ModelAdmin):
    pass
