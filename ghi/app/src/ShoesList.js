import {loadShoes} from './index'

function ShoesList(props) {
    const handleDeleteShoe = async(event, shoeId) => {
        event.preventDefault();
        const url = `http://localhost:8080/api/shoes/${shoeId}`
        const fetchConfig = {
            method: "DELETE"
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            const reply = await response.json();
            loadShoes();
        }
    }
    return (
<div>
            <br></br>
            <table className="table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Picture</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Bin's Closet Name</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td><img src={shoe.picture_url} className="rounded" width="50" height="50" alt="img"/></td>
                                <td>{shoe.manufacturer_name}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.shoes_color}</td>
                                <td>{shoe.bin.closet_name}</td>
                                <td><button type="button" className="btn btn-outline-danger"onClick={(event) => {
                                    handleDeleteShoe(event,shoe.id)
                                }}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ShoesList;
