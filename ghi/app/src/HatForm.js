import React, { useEffect, useState} from 'react';

function HatForm(){
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [style_name, setStyle] = useState('');
    const [picture_url, setPictureUrl] = useState([]);
    const [location, setLocation] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={}
        data.color = color;
        data.style_name = style_name;
        data.fabric = fabric;
        data.location = location;
        data.picture_url = picture_url;
        const url = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if(response.ok) {
            event.target.reset();
            setStyle('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
            
        }
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a New Hat... or don't.</h1>
                <form onSubmit={(event) => handleSubmit(event)} id="create-hat-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleStyleChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control" />
                    <label htmlFor="style_name">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric type</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture Url</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                        <option value="">Select a wardrobe</option>
                        {locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>{location.closet_name}</option>
                        )
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm;
