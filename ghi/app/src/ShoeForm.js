import React, { useEffect, useState} from 'react';
import {loadShoes} from './index'

function ShoeForm(){
    const [bins, setBins] = useState([]);
    const [model_name,setName] = useState('');
    const [shoes_color, setColor] = useState('');
    const [manufacturer_name,setManufacturer] = useState('');
    const [picture_url, setPictureUrl] = useState([]);
    const [bin, setBin] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={}
        data.shoes_color = shoes_color;
        data.model_name = model_name;
        data.manufacturer_name = manufacturer_name;
        data.bin = bin;
        data.picture_url = picture_url;
        const url = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if(response.ok) {
            event.target.reset();
            setName('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
            setBin('');
            loadShoes();
        }
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a New Shoe</h1>
                <form onSubmit={(event) => handleSubmit(event)} id="create-shoe-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                    <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} placeholder="Manufacturer Name" required type="text" name="manufacturer_name" id="manufacturer_name" className="form-control" />
                    <label htmlFor="manufacturer_name">Manufacturer Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required type="text" name="shoes_color" id="shoes_color" className="form-control" />
                    <label htmlFor="shoes_color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture Url</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                        )
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    )
}

export default ShoeForm;
