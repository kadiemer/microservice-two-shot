import React, {useEffect, useState} from "react";

function HatsList() {
    const [hats, sethats] = useState([]);
    const handleDeleteHat = async(event, hatId) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/hats/${hatId}`
        const fetchConfig = {
            method: "DELETE"
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            loadHats();
        }
    }
    const loadHats = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok){
          const data = await response.json();
          sethats(data.hats)
        } else {
          console.error(response);
        }
    }

    useEffect(() => {
        loadHats();
    }, []);

    return (
        <div>
            <br></br>
            <table className="table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Picture</th>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>wardrobe name</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.href}>
                                <td><img src={hat.picture_url} className="rounded" width="50" height="50" alt="img"/></td>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{hat.color}</td>
                                <td>{hat.location.closet_name}</td>
                                <td><button type="button" className="btn btn-outline-danger" onClick={(event) => {
                                    handleDeleteHat(event, hat.id)
                                }}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default HatsList;
